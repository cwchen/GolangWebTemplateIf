# [Golang][Web] If in http/template

A tiny demo app for `if` in [http/template](https://golang.org/pkg/html/template/).

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/GolangWebTemplateIf.git
```

Run this program:

```
$ cd GolangWebTemplateIf
$ go run main.go
```

Visit http://localhost:8080 for the result:

![Using if in http/template](images/golang-web-template-if.PNG)

## Copyright

2018, Michael Chen; Apache 2.0.
